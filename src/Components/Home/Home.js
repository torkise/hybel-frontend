import React, { useState, Image } from "react";
import "./Home.css";
import photo from './Image-2.jpg'

const Home = (props) => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <div id="trd_photo">
     <img src={photo}/>
    </div>
  );
};

export default Home;
