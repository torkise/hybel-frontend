import React, { useEffect, useState, useRef } from "react";
import {
  Dropdown,
  Button,
  Form,
  OverlayTrigger,
  Tooltip,
  Table,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Filter.css";
import axios from "axios";

// Filter er flytverdi på nettsida
const Filter = (props) => {
  const skoler = [
    {
      _id: 1,
      name: "Kalvkinnet",
      longitude: 10.3874,
      latitude: 63.4284,
    },
    {
      _id: 2,
      name: "Gløshaugen",
      longitude: 10.4033,
      latitude: 63.4184,
    },
    {
      _id: 3,
      name: "Dragvoll",
      longitude: 10.4702,
      latitude: 63.4083,
    },
  ];

  const [valgtSkole, setValgtSkole] = useState(["Velg Skole"]);
  const [avstandSkoleFlytVerdi, setAvstandSkoleFlytVerdi] = useState(0.5);
  const [avstandSkoleFlytVerdiAktiv, setAvstandSkoleFlytVerdiAktiv] =
    useState(true);

  const [valgtPris, setValgtPris] = useState("Velg prisklasse");
  const [prisFlytVerdi, setPrisFlytVerdi] = useState(0.5);
  const [prisAktiv, setPrisAktiv] = useState(true);

  const [hybels, setHybels] = useState([]);
  const [sortetHybels, setSortedHybels] = useState([]);

  const [tableInactive, setTableInactive] = useState(true);

  useEffect(() => {
    let data;
    axios
      .get("http://localhost:8000/api/view/?format=json")
      .then((res) => {
        setHybels(res.data);
      })
      .catch((err) => {});
  }, []);

  const [hybelFlyt, setHybelFlyt] = useState([]);

  function getColorCode(floatValue) {
    if (floatValue > 0.75) {
      return <i class="bi bi-circle-fill" id="green_circle"></i>;
    } else if (floatValue > 0.4) {
      return <i class="bi bi-circle-fill" id="yellow_circle"></i>;
    }
    return <i class="bi bi-circle-fill" id="red_circle"></i>;
  }

  function calculateFloat() {
    //Iterer over listen med hybler, og bruk verdiene fra slidere til å kalkulere en flytverdi
    setHybelFlyt([]);
    hybels.map((hybel) => {
      let floatVal = calculateFloatValue(hybel);
      let colorCode = getColorCode(floatVal);
      let h = {
        pris: hybel.pris,
        adresse: hybel.adresse,
        color: colorCode,
        floatVal: floatVal,
      };
      setHybelFlyt((hybelFlyt) => [...hybelFlyt, h]);
    });

    //console.log("Hyblene", hybelFlyt);
    let unsortedHybel = [...hybelFlyt];
    // console.log("Usortert", unsortedHybel);
    unsortedHybel.sort((a, b) => {
      return b.floatVal - a.floatVal;
    });
    //console.log("sorter", unsortedHybel);
    setSortedHybels(unsortedHybel);

    //her må den kalle på noe som oppdaterer hvordan det vises på nettsiden
  }

  useEffect(() => {
    calculateFloat();
  });

  function getDistanceFromLatLonInKm(hybel) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(valgtSkole[2] - hybel.latitude);
    var dLon = deg2rad(valgtSkole[1] - hybel.longitude);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(hybel.latitude)) *
        Math.cos(deg2rad(valgtSkole[2])) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  function deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  function getDistanceValue(dist) {
    if (dist <= 1.5) {
      return 1;
    } 
    else if (dist > 1.5 && dist < 4){
      return -0.4*dist + 1.6
    } else {
      return 0;
    }
  }

  function getPriceValue(price) {
    if (price <= 4500) {
      return 1;
    } else if (price > 4500 && price < 7000){
      return -1/2500*price+14/5
    } else {
      return 0;
    }
  }

  function getWeigths() {
    if (!avstandSkoleFlytVerdiAktiv && !prisAktiv) {
      return [
        avstandSkoleFlytVerdi / (avstandSkoleFlytVerdi + prisFlytVerdi),
        prisFlytVerdi / (avstandSkoleFlytVerdi + prisFlytVerdi),
      ];
    } else if (avstandSkoleFlytVerdiAktiv && prisAktiv) {
      return [0, 0];
    } else if (avstandSkoleFlytVerdiAktiv && !prisAktiv) {
      return [0, 1];
    } else if (!avstandSkoleFlytVerdiAktiv && prisAktiv) {
      return [1, 0];
    }
  }

  function calculateFloatValue(hybel) {
    let distance = getDistanceFromLatLonInKm(hybel);
    let price = hybel.pris;
    let distWeight = getWeigths()[0];
    let priceWeight = getWeigths()[1];
    const returnValue = (
      priceWeight * getPriceValue(price) +
      distWeight * getDistanceValue(distance)
    ).toFixed(4);
    return returnValue; 
  }

  //HTML kode nedover
  const kalkulator = (
    <div className="container-fluid">
      <h3 className="header-filter" inline>
        Flytverdikalulator
      </h3>
      <h6>
        Velg hvor viktig du synes pris og avstand til ønsket skole er ved å
        bevege på sliderene, og få en flytverdi som gir deg hybelen som passer
        best til dine ønsker. Viktighet 1 er mest viktig og viktighet 0 er minst
        viktig.{" "}
      </h6>

      <Form>
        <Form.Check
          type="switch"
          id="custom-switch-avstand"
          label="Nærhet til Skole"
          onClick={() =>
            setAvstandSkoleFlytVerdiAktiv(!avstandSkoleFlytVerdiAktiv)
          }
        />
        <Dropdown>
          <Dropdown.Toggle inline variant="secondary" id="dropdown-basic">
            {valgtSkole[0]}
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {skoler.map((type) => (
              <Dropdown.Item
                onClick={() =>
                  setValgtSkole([type.name, type.longitude, type.latitude])
                }
              >
                {type.name}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
        <Form.Label>Viktighet: {avstandSkoleFlytVerdi} </Form.Label>
        <Form.Range
          id="skole-range"
          disabled={avstandSkoleFlytVerdiAktiv}
          onChange={() => {
            const e = document.getElementById("skole-range");
            setAvstandSkoleFlytVerdi(e.value / 100);
          }}
        ></Form.Range>
      </Form>

      <Form>
        <Form.Check
          type="switch"
          id="custom-switch"
          label="Billig leie"
          onClick={() => setPrisAktiv(!prisAktiv)}
        />
        <Form.Label>Viktighet: {prisFlytVerdi} </Form.Label>
        <Form.Range
          id="pris-range"
          disabled={prisAktiv}
          onChange={() => {
            const e = document.getElementById("pris-range");
            setPrisFlytVerdi(e.value / 100);
          }}
        ></Form.Range>
      </Form>

      <Button
        className="filter-btn"
        variant="secondary"
        size="lg"
        onClick={() => {
          console.log(tableInactive);
          calculateFloat();
          setTableInactive(false);
        }}
      >
        <h5>Vis</h5>
      </Button>
    </div>
  );

  const tabell = (
    <div id="tabell-container">
      <h5>
        Resultatet vises i tabellen under. Høy flytverdi tilsier god match med
        dine kriterier.
      </h5>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Flytverdi</th>
            <th>Adresse</th>
            <th>Pris</th>
          </tr>
        </thead>
        <tbody>
          {
            //denne oppdaterer seg ikke tidlig nok
            sortetHybels.map((hybel) => (
              <tr>
                <th>
                  {" "}
                  {hybel.color} {hybel.floatVal}
                </th>
                <th>{hybel.adresse}</th>
                <th>{hybel.pris}</th>
              </tr>
            ))
          }
        </tbody>
      </Table>
    </div>
  );

  return (
    <div>
      <div className="container-fluid-value">{kalkulator}</div>
      <div
        className={`container-fluid-tabell ${tableInactive ? "inactive" : ""}`}
      >
        <div>{tabell}</div>
      </div>
    </div>
  );
};

export default Filter;
