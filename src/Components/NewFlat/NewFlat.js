import React, { Fragment, useEffect, useState } from "react";
import "./NewFlat.css";
import axios from "axios";
import { Form, Col, Row } from "react-bootstrap";

const NewFlat = (props) => {
  const [adresse, setAdresse] = useState(null);
  const [nummer, setNummer] = useState(null);
  const [pris, setPris] = useState(null);
  const [antallSoverom, setAntallSoverom] = useState(null);
  const [antallLedigeSoverom, setLedigeSoverom] = useState(null);
  const [beskrivelse, setBeskrivelse] = useState(null);
  const [email, setEmail] = useState(null);

  const [queryResult, setQueryresult] = useState([]);
  const [choosenValue, setChoosenValue] = useState([]);

  const addHybel = async () => {
    let errorThrown = false;
    if (
      pris === null ||
      pris === "" ||
      antallLedigeSoverom === null ||
      antallLedigeSoverom === "" ||
      antallSoverom === null ||
      antallSoverom === ""
    ) {
      alert("Alle feltene må fylles ut");
      errorThrown = true;
      return null;
    }
    if (parseInt(antallLedigeSoverom) > parseInt(antallSoverom)) {
      alert("Kan ikke leie ut flere rom enn du har. ");
      errorThrown = true;
      return null;
    }
    if (choosenValue == null || choosenValue == "") {
      alert("Fant ikke adressen");
      errorThrown = true;
      return null;
    }

    axios
      .post("http://127.0.0.1:8000/api/create/", {
        longitude: choosenValue.representasjonspunkt.lon,
        latitude: choosenValue.representasjonspunkt.lat,
        pris: parseInt(pris),
        antallLedigeSoverom: parseInt(antallLedigeSoverom),
        beskrivelse: beskrivelse,
        adresse: choosenValue.adressetekst,
        epost: email
      })
      .then((response) => {
        console.log(response.data); // view the response
      })
      .catch((error) => {
        console.log(error);
        alert("Error: Annsonse feilet");
        errorThrown = true;
        // check if any error
      });
    if (!errorThrown) {
      alert("Annonsen er lagt ut");
    }
  };

  function resetAll() {
    setAdresse("");
    setNummer("");
    setAntallSoverom("");
    setBeskrivelse("");
    setLedigeSoverom("");
    setPris("");
    setEmail("");
  }

  function search_adress() {
    const urlGEO = `https://ws.geonorge.no/adresser/v1/sok?side=0&treffPerSide=1000&asciiKompatibel=true&utkoordsys=4258&sok=${adresse}`;
    let data;
    axios
      .get(urlGEO)
      .then((res) => {
        setQueryresult(res.data.adresser);
      })
      .catch((err) => {});
    queryResult.map((res) => {
      if (res.kommunenavn == "TRONDHEIM") {
      }
    });
  }

  function find_correct_house() {
    if (queryResult.length < 1) return;
    queryResult.map((res) => {
      if (res.kommunenavn == "TRONDHEIM") {
        if (res.nummer == parseInt(nummer)) {
          setChoosenValue(res);
        }
      }
    });
  }

  useEffect(() => {
    find_correct_house();
  });

  return (
    <div className="container-fluid-table">
      <h3> Opprett ny annonse</h3>
      <h6>Fyll inn informasjon om din boligannonse nedenfor.</h6>
      <Form className="form-group">
        <Form.Control
          type="text"
          required
          placeholder="Email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Row>
          <Col>
            <Form.Control
              type="text"
              required
              placeholder="Gatenavn"
              name="adresse"
              value={adresse}
              onChange={(e) => {
                setAdresse(e.target.value);
                search_adress();
                console.log(queryResult);
              }}
              onKeyPress={(event) => {
                if (event.key === "Enter") {
                  search_adress();
                }
              }}
            />
          </Col>
          <Col>
            <Form.Control
              className="gate-nr"
              type="text"
              required
              placeholder="Gatenummer"
              inline
              name="nummer"
              value={nummer}
              onChange={(e) => {
                setNummer(e.target.value);
                find_correct_house();
              }}
            />
          </Col>
        </Row>
        <Form.Control
          type="text"
          required
          placeholder="Pris"
          name="pris"
          value={pris}
          onChange={(e) => setPris(e.target.value)}
        />
        <Form.Control
          type="text"
          required
          placeholder="Antall Rom"
          name="antallSoverom"
          value={antallSoverom}
          onChange={(e) => setAntallSoverom(e.target.value)}
        />
        <Form.Control
          type="text"
          required
          placeholder="Ledige Soverom"
          name="ledigeSoverom"
          value={antallLedigeSoverom}
          onChange={(e) => setLedigeSoverom(e.target.value)}
        />
        <Form.Control
          type="text"
          required
          placeholder="Beskrivelse"
          name="beskrivelse"
          value={beskrivelse}
          onChange={(e) => setBeskrivelse(e.target.value)}
        />
        <button className="btn btn-success" onClick={addHybel}>
          Legg ut bolig{" "}
        </button>
        <button className="btn btn-success" onClick={() => resetAll()}>
          Nullstill
        </button>
      </Form>
    </div>
  );
};

export default NewFlat;
