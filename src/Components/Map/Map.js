import React, { useRef, useEffect, useState } from "react";
import {
  Dropdown,
  Button,
  OverlayTrigger,
  Tooltip,
  FormControl,
  InputGroup,
} from "react-bootstrap";
import axios from "axios";
import "./Map.css";
import "./navbar.css"
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import "@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css";
import * as MapboxGeocoder from "@mapbox/mapbox-gl-geocoder";
mapboxgl.accessToken =
  "pk.eyJ1IjoiaGVsZW5yZWIiLCJhIjoiY2t6ZHdzd3NpMGVnNDMycGQ5dGxzZng4MiJ9.2-fC3a6jg0gEbbXHeMOkaQ";

export const studiesteder = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [10.3893, 63.4282],
      },
      properties: {
        title: "Kalvkinnet",
        description: "Campus NTNU Kalvkinnet",
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [10.4033, 63.4184],
      },
      properties: {
        title: "Gløshaugen",
        description: "Campus NTNU Gløshaugen",
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [10.4702, 63.4083],
      },
      properties: {
        title: "Dragvoll",
        description: "Campus NTNU Dragvoll",
      },
    },
  ],
};

export default function Map() {
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng, setLng] = useState(10.398);
  const [lat, setLat] = useState(63.424);
  const [zoom, setZoom] = useState(13);
  const [switchedStyle, setSwitchedStyle] = useState(false);

  const [hybels, setHybels] = useState([]);
  const [markers, setMarkers] = useState([]);

  const avstandItems = [
    { id: "1", name: "Gløshaugen", coordinates: [10.4033, 63.4184] },
    { id: "2", name: "Kalvkinnet", coordinates: [10.3893, 63.4282] },
    { id: "3", name: "Dragvoll", coordinates: [10.4702, 63.4083] },
  ];

  function getMarkerColor() {
    if(switchedStyle == true) {
      return "#6fb1bc"
    }
    return "#188c7b"
  }


  useEffect(() => {
    let data;
    axios
      .get("http://localhost:8000/api/view/?format=json")
      .then((res) => {
        setHybels(res.data);
      })
      .catch((err) => {});
  }, []);

  function searchForHybels() {
    markers.map((mark) => mark.remove());
    hybels.map((hybel) => {
      const marker = new mapboxgl.Marker({
        color: getMarkerColor(),
        scale: 0.6,
      })
        .setLngLat([hybel.longitude, hybel.latitude])
        .setPopup(createHybelPopup(hybel.pris, hybel.beskrivelse, hybel.adresse, hybel.epost));

      if (
        checkFilter(
          hybel.pris,
          hybel.longitude,
          hybel.latitude,
          hybel.adresse,
          hybel.beskrivelse
        )
      ) {
        markers.push(marker);
        marker.addTo(map.current);
      } else {
      }
    });
  }

  useEffect(() => {
    searchForHybels();
  });

  function checkFilter(pris, long, lat, adresse, beskrivelse) {
    if (miniprisActive) {
      if (!isPriceOk("min", pris)) return false;
    }
    if (maksprisActive) {
      if (!isPriceOk("maks", pris)) return false;
    }
    if (avstandValueActive) {
      if (!isDistanceOK(long, lat)) return false;
    }
    if (nokkelOrdList.length > 0) {
      if (!isKeyWordOK(adresse, beskrivelse)) return false;
    }
    return true;
  }

  function isKeyWordOK(adresse, beskrivelse) {
    let adresseKeyWords = adresse.split(" ");
    let beskrivelseKeyWords = beskrivelse.split(" ");
    let antMatch = 0;
    nokkelOrdList.map((nokkelOrd) => {
      adresseKeyWords.map((adresse) => {
        if (nokkelOrd.toLowerCase().trim() === adresse.toLowerCase().trim()) {
          antMatch = antMatch + 1;
        }
      });
      beskrivelseKeyWords.map((beskrivelse) => {
        if (
          nokkelOrd.toLowerCase().trim() === beskrivelse.toLowerCase().trim().replace(',', '').replace('.', '').replace('!', '').replace('?', '')
        ) {
          antMatch = antMatch + 1;
        }
      });
    });
    // Må matche minst 60%
    if (antMatch < nokkelOrdList.length * 0.6) return false;
    return true;
  }

  function isDistanceOK(long, lat) {
    let coordsFra = null;

    avstandItems.map((item) => {
      if (item.name == avstandFra) {
        coordsFra = item.coordinates;
      }
    });

    if (coordsFra == null) return true;
    if (calculateDistance([long, lat], coordsFra) > avstandValue) return false;

    return true;
  }

  function calculateDistance(coordHybel, coordsFra) {
    var R = 6371; // Radius of the earth in km
    var dLon = deg2rad(coordHybel[0] - coordsFra[0]);
    var dLat = deg2rad(coordHybel[1] - coordsFra[1]);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(coordHybel[1])) *
        Math.cos(deg2rad(coordsFra[1])) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;

  }

  function deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  function isPriceOk(minMaks, hybelPris) {
    if (minMaks == "min") {
      if (hybelPris < minprisValue) return false;
    } else {
      if (hybelPris > maksprisValue) return false;
    }

    return true;
  }  
  

  function createHybelPopup(pris, beskrivelse, adresse, epost) {
    const htmlCode = 
         `<div id = "main">
          <div id = "title">
          <h5>${adresse}</h5> 
          </div>
          <div id = "info">
          <h6> Pris: ${pris} NOK </h6> 
          <h7 id = "b"> ${beskrivelse}</h7> 
          <br>
          <br>
          <br>
          <h7> Kontakt: ${epost}</h7>
          </div>
          </div>`
    const popup = new mapboxgl.Popup().setHTML(htmlCode);
    return popup;
  }

  useEffect(() => {
    if (map.current) return; // initialize map only once
    //setCenter();
    console.log("Initializing map");
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v10",
      center: [lng, lat],
      zoom: zoom,
      
      
    });
    searchForHybels();

    map.current.addControl(
      new mapboxgl.FullscreenControl({
        container: document.querySelector("body"),
      }),
      "bottom-right"
    );
    
  });

  useEffect(() => {
    if (!map.current) return; // wait for map to initialize
    map.current.on("load", () => {
      searchForHybels();
    });
    map.current.on("move", () => {
      setLng(map.current.getCenter().lng.toFixed(4));
      setLat(map.current.getCenter().lat.toFixed(4));
      setZoom(map.current.getZoom().toFixed(2));
    });
  });

  const [avstandFra, setAvstandFra] = useState("Velg Campus");
  const [avstandValueActive, setAvstandValueActive] = useState(false);
  const [miniprisActive, setminiprisActive] = useState(false);
  const [maksprisActive, setmaksprisActive] = useState(false);
  const [avstandValue, setAvstandValue] = useState("");
  const [minprisValue, setMiniprisValue] = useState("");
  const [maksprisValue, setMaksprisValue] = useState("");
  const [nokkelOrd, setNokkelOrd] = useState("");
  const [nokkelOrdList, setNokkelOrdList] = useState([]);

  function resetFilter() {
    setAvstandFra("Velg Campus");
    setAvstandValueActive(false);
    setminiprisActive(false);
    setmaksprisActive(false);
    setAvstandValue("");
    setMiniprisValue("");
    setMaksprisValue("");
    setNokkelOrd("");
    setNokkelOrdList([]);
  }

  const filter = (
    <div className="container-fluid-filter">
      <h4>
        Filter
        <Button
          className="nullstill-btn"
          variant="secondary"
          inline
          onClick={() => resetFilter()}
        >
          Nullstill Filter
        </Button>
      </h4>

      <h5 inline>Maks Avstand</h5>
      <Dropdown inline>
        <Dropdown.Toggle variant="secondary" id="dropdown-filter">
          {avstandFra}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {avstandItems.map((type) => (
            <Dropdown.Item
              onClick={() => {
                setAvstandFra(type.name);
                searchForHybels();
              }}
            >
              {type.name}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>

      <label for="customRange1" class="form-label">
        <input
          type="checkbox"
          checked={avstandValueActive}
          inline
          onClick={() => setAvstandValueActive(!avstandValueActive)}
        />
        {avstandFra}: {avstandValue} km
      </label>
      <input
        type="range"
        class="form-range"
        id="range-avstand"
        min="0"
        max="10"
        step="0.2"
        disabled={!avstandValueActive}
        onChange={() => {
          const e = document.getElementById("range-avstand");
          setAvstandValue(e.value);
          searchForHybels();
        }}
      />

      <h5>Makspris</h5>
      <label for="customRange1" class="form-label">
        <input
          type="checkbox"
          inline
          checked={maksprisActive}
          onClick={() => setmaksprisActive(!maksprisActive)}
        />
        Maxpris:{maksprisValue}
      </label>
      <input
        type="range"
        class="form-range"
        id="range-maks-pris"
        min="0"
        max="20000"
        step="1000"
        disabled={!maksprisActive}
        onChange={() => {
          const e = document.getElementById("range-maks-pris");
          setMaksprisValue(e.value);
          searchForHybels();
        }}
      />
      <h5>Minstepris</h5>
      <label for="customRange1" class="form-label">
        <input
          type="checkbox"
          inline
          checked={miniprisActive}
          onClick={() => setminiprisActive(!miniprisActive)}
        />
        Minstepris:{minprisValue}
      </label>
      <input
        type="range"
        class="form-range"
        id="range-mini-pris"
        min="500"
        max="20000"
        step="500"
        disabled={!miniprisActive}
        onChange={() => {
          const e = document.getElementById("range-mini-pris");
          setMiniprisValue(e.value);
          searchForHybels();
        }}
      />
      <h5>Nøkkelord</h5>
      <InputGroup size="sm" className="mb-3">
        <FormControl
          type="text"
          placeholder="Nøkkelord"
          aria-label="Nøkkelord"
          id="nokkel-ord"
          value={nokkelOrd}
          onChange={(e) => {
            setNokkelOrd(e.target.value);
          }}
          onKeyPress={(event) => {
            if (event.key === "Enter") {
              setNokkelOrdList((nokkelOrdList) => [
                ...nokkelOrdList,
                nokkelOrd,
              ]);
              setNokkelOrd("");
            }
          }}
        ></FormControl>
      </InputGroup>
      {nokkelOrdList.map((type) => (
        <map>{type}, </map>
      ))}
    </div>
  );

  return (
    <div>
      <OverlayTrigger trigger="click" placement="bottom" rootClose="true" overlay={filter}>
        <Button className="filter-btn">Filter</Button>
      </OverlayTrigger>
      <Button
        onClick={() => {
          if (switchedStyle) {
            map.current.setStyle("mapbox://styles/mapbox/streets-v10");
            setSwitchedStyle(!switchedStyle);
          } else {
            map.current.setStyle("mapbox://styles/mapbox/dark-v10");
            setSwitchedStyle(!switchedStyle);
          }
        }}
        className="maptype-btn"
      >
        {" "}
        Dag/Natt{" "}
      </Button>
      <div id="map" ref={mapContainer} className="map-container" />
    </div>
  );
}
