import React, { useState, useEffect } from "react";
import "./Sidebar.css";
import { NavLink, Link } from "react-router-dom";
import { Dropdown, Form, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import MenuItem from "./MenuItem";
import { type } from "@testing-library/user-event/dist/type";

export const menuItems = [
  // Her legger du til nye elementer til menyen!
  {
    name: "Hjemme",
    exact: true,
    to: "/",
    iconClassName: "bi bi-house-heart",
  },
  {
    name: "Kart",
    excat: "true",
    to: "/map",
    iconClassName: "bi bi-map",
  },
  {
    name: "Ny Annonse",
    excat: true,
    to: "/ny-annonse",
    iconClassName: "bi bi-plus-square",
  },
  {
    name: "Flytverdikalkulator",
    excat: true,
    to: "/filter",
    iconClassName: "bi bi-filter-square",
  },
  
];
export const avstandItems = [
  { name: "Gløshaugen" },
  { name: "Kalvkinnet" },
  { name: "Dragvoll" },
  { name: "Sett ut Markør" },
];



const SideBar = (props) => {
  const [inactive, setInactive] = useState(false);
  const [middleInactive, setMiddleInactive] = useState(true)
  const [avstandFra, setAvstandFra] = useState("Avstand");

  const [avstandValueActive, setAvstandValueActive] = useState(false);
  const [miniprisActive, setminiprisActive] = useState(false);
  const [maksprisActive, setmaksprisActive] = useState(false);
  const [avstandValue, setAvstandValue] = useState("");
  const [minprisValue, setMiniprisValue] = useState("");
  const [maksprisValue, setMaksprisValue] = useState("");


  useEffect(() => {
    if (inactive) {
      removeActiveClassFromSubMenu();
    }

    props.onCollapse(inactive);
  }, [inactive]);
  
  

  const removeActiveClassFromSubMenu = () => {
    document.querySelectorAll(".sub-menu").forEach((el) => {
      el.classList.remove("active");
    });
  };

  useEffect(() => {
    let menuItems = document.querySelectorAll(".menu-item");
    menuItems.forEach((el) => {
      el.addEventListener("click", (e) => {
        const next = el.nextElementSibling;
        removeActiveClassFromSubMenu();
        menuItems.forEach((el) => el.classList.remove("active"));
        el.classList.toggle("active");
        console.log(next);
        if (next !== null) {
          next.classList.toggle("active");
        }
      });
    });
  }, []);

  

  return (
    <div className={`side-bar ${inactive ? "inactive" : ""}`}>
      
      <div className="top-section">
        

        <div onClick={() => setInactive(!inactive)} className="toggle-menu-btn">
          {inactive ? (
            <i class="bi bi-arrow-right-square-fill" />
          ) : (
            <i class="bi bi-arrow-left-square-fill" />
          )}
        </div>
        
        <div className="divider"></div>

        <div className="main-menu">
          <ul>
            {menuItems.map((menuItem, index) => (
              <MenuItem
                key={index}
                name={menuItem.name}
                exact={menuItem.exact}
                to={menuItem.to}
                subMenus={menuItem.subMenus || []}
                iconClassName={menuItem.iconClassName}
                onClick={(e) => {
                  if (inactive) {
                    setInactive(false);
                  }
                  if(menuItem.name == "Kart") {
                      setMiddleInactive(false)
                  }
                  
                  else {
                    setMiddleInactive(true)
                  }
                }}
              />
            ))}
          </ul>
        </div>
      </div>
      <div className="divider"></div>
     
      <div className="side-menu-footer">
        <h3>Kontakt oss</h3>
        <div>Vi skal utvide bedriften vår, send mail for interesse:</div>

        <div><b>Frontend-utviklere:</b> helenreb@stud.ntnu.no</div>
        <div>ingviah@stud.ntnu.no</div>
        <div><b>Backend-utvikler:</b> torkise@stud.ntnu.no</div>  
        <div><b>Korrektur-leser:</b> simonen@stud.ntnu.no</div>  
        <div><b>HR:</b> erlenstv@stud.ntnu.no</div>            
      </div>
    </div>
  );
};

export default SideBar;
