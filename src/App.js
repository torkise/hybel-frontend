import Map from "../src/Components/Map/Map";
import SideBar from "./Components/Sidebar/Sidebar";
import NewFlat from "./Components/NewFlat/NewFlat";
import Home from "../src/Components/Home/Home"
import Filter from "./Components/Filter/Filter";
import React, {useState} from "react";
import "./App.css";
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
mapboxgl.accessToken =
  "pk.eyJ1IjoiaGVsZW5yZWIiLCJhIjoiY2t6ZHdzd3NpMGVnNDMycGQ5dGxzZng4MiJ9.2-fC3a6jg0gEbbXHeMOkaQ";

const HomePage = () => {
  return <Home/>;
};

const NewFlatPage = () => {
  return <NewFlat />;
};

const MapDeskop = () => {
  return <Map />;
};




const FilterDeskop = () => {
  return <Filter/> 
}

//Bruk denne når dere styler Ingvild/Helna/Simon 
//hahah simon du kommer ikke til å lese dette 
//https://webaim.org/resources/contrastchecker/

function App() {
  const [inactive, setInactive] = useState(false);
  return (
    <div>
      <Router>
        <SideBar
          onCollapse={(inactive) => {
            console.log(inactive);
            setInactive(!inactive);
          }}
        />
        <div className={`container ${!inactive ? " inactive" : ""}`}>
          <Routes>
            <Route exact path="/" element={<HomePage />} ></Route>
            <Route exact path="/Map" element={<MapDeskop />} ></Route>
            <Route exact path="/filter" element={<FilterDeskop />}></Route>
            <Route exact path="/ny-annonse" element={<NewFlatPage />}></Route>
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
